# [1.48.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.47.0...v1.48.0) (2020-10-08)


### Features

* **pagination:** Update pagination with page counters information ([9df1bb7](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/9df1bb7a3cb86257f0c3a89a621c77ee6758113a))

# [1.47.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.46.2...v1.47.0) (2020-10-06)


### Features

* **buttons:** Add info about icon only loading button ([e6642e6](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/e6642e6ac383e4b6ca0317cea15f6cf544e4d2b4))

## [1.46.2](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.46.1...v1.46.2) (2020-10-05)


### Bug Fixes

* **deps:** upgrade Nuxt to v2.14.6 ([982106d](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/982106ddb35de917e7785ca24d6387ff254ff3e3))

## [1.46.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.46.0...v1.46.1) (2020-10-02)


### Reverts

* Revert "Update nuxt.config.js" ([83da4b2](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/83da4b2f549f2387744a5371b29cdcb7ebb17144))

# [1.46.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.45.0...v1.46.0) (2020-10-02)


### Features

* Add initial gitpod configuration files ([a6ccb40](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/a6ccb40c2672dace3286e86b66b7f200c0a7562f))

# [1.45.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.44.1...v1.45.0) (2020-09-30)


### Features

* **Tabs:** Updating fitted tab guidelines ([4a6eb8f](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/4a6eb8fc398e030df055a9f73e35b25397c88aed))

## [1.44.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.44.0...v1.44.1) (2020-09-30)


### Bug Fixes

* **buttons:** Update guidance on button positioning ([1d17a37](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/1d17a37e7413d4f8f711d504371c5b12f9d3d5b5))

# [1.44.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.43.1...v1.44.0) (2020-09-29)


### Features

* **Forms:** Document different validation types ([db3cf22](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/db3cf2274d15ec20964db0cd837e533ff34eb6f9))

## [1.43.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.43.0...v1.43.1) (2020-09-24)


### Bug Fixes

* **checkbox:** Remove horizontal layout option ([e59aab0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/e59aab0f46608bb6d02401d381cad09d99c3899f))

# [1.43.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.42.0...v1.43.0) (2020-09-21)


### Features

* **GLButton:** Allow for a neutral button to be the primary action ([36b09eb](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/36b09ebecd637386c6f8e7553caef3302690aa7a))

# [1.42.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.41.0...v1.42.0) (2020-09-09)


### Features

* **navigation:** define navigation related terms ([d68b212](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/d68b212867a840fc680346b39998ad46ff865c2f))

# [1.41.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.40.2...v1.41.0) (2020-09-09)


### Features

* **Color:** Update purple 600 hex ([aa628fd](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/aa628fd5877212defc3bbdfbbd5dca14b8ddd2eb))

## [1.40.2](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.40.1...v1.40.2) (2020-09-03)


### Bug Fixes

* **skeleton-loader:** Update link to carbon DS loading guidelines ([f7d599a](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/f7d599ab8b1f7247cb510554b418394c985ffa46))

## [1.40.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.40.0...v1.40.1) (2020-09-02)


### Bug Fixes

* Correct link to dropdown specs in Figma ([0438b6f](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/0438b6f0562d47848a27e1c4d65e1c9e8cd8595a))

# [1.40.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.39.1...v1.40.0) (2020-08-27)


### Features

* **contribute:** Add conventional commits to get started ([fd267d3](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/fd267d33efdb2a5333eecb8ef15a5208d00d68c3))

## [1.39.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.39.0...v1.39.1) (2020-08-24)


### Bug Fixes

* **tree:** Update broken link accordionS to accordion ([126b8e3](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/126b8e364c6b05fc540100d0e2623b6077aedf2e))

# [1.39.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.38.0...v1.39.0) (2020-08-20)


### Features

* **alert:** Add guidelines for global alerts ([a772a8e](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/a772a8ea15bb8257c17c35d7dd22e59d5c0b4248))

# [1.38.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.37.1...v1.38.0) (2020-08-19)


### Features

* **layers:** Add layers documentation ([862679f](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/862679f1c5fc77824bfb4da9b8d8759261d67742))

## [1.37.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.37.0...v1.37.1) (2020-08-19)


### Bug Fixes

* **GlDropdown:** Update GlNewDropdown with GlDropdown ([3b6f171](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/3b6f17154c071d14aabd45f003f2b457e0c87cd2))

# [1.37.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.36.0...v1.37.0) (2020-08-18)


### Features

* **Buttons:** Clarify icon use in default dropdown button ([bca9210](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/bca92109066305fa36311409046fea5b395f9503))

# [1.36.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.35.0...v1.36.0) (2020-08-11)


### Features

* **forms:** Add recommended Enter key behavior ([b2aa4a4](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/b2aa4a4c66b06e7e5e19ea761517da846f8851bc))

# [1.35.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.34.0...v1.35.0) (2020-08-04)


### Features

* **content:** Clarify case for features that are also objects ([eb3286a](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/eb3286a24bac073a478b09053b48ac2eba5c05ed))

# [1.34.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.33.0...v1.34.0) (2020-08-03)


### Features

* **alerts:** Document that alerts should be actionable ([393997f](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/393997fd4dba82956e3d751bbedac3d4c6649301))

# [1.33.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.32.0...v1.33.0) (2020-08-03)


### Features

* **Charts:** add popover text wrap info ([fe1443b](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/fe1443b4ffd21f79aae4dd1ca0e71bb8d4a1a84e))

# [1.32.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.31.1...v1.32.0) (2020-07-24)


### Features

* **Buttons:** Including button group example ([087e60a](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/087e60aecdfd59b1177d8f190737a8e3c0433d78))

## [1.31.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.31.0...v1.31.1) (2020-07-23)


### Bug Fixes

* bind aria-expanded to open property ([6a4598f](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/6a4598f9245db00e4b461005aafc9a212abd80ec))

# [1.31.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.30.0...v1.31.0) (2020-07-23)


### Features

* **dropdowns:** Add Figma spec links ([7a66d53](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/7a66d53f1a6ca95715c41c524261f418839c57cc))

# [1.30.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.29.0...v1.30.0) (2020-07-21)


### Features

* **drag-drop:** Add more comprehensive docs for drag drop ([44e7ed7](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/44e7ed7059b50dea2a6cd5da875e47adcfff878e))

# [1.29.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.28.0...v1.29.0) (2020-07-20)


### Features

* **charts:** Add grouped stacked column chart docs ([75c3288](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/75c3288001ccb2fd1f450a79c1b26c6bdf6e0cf5))

# [1.28.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.27.0...v1.28.0) (2020-07-20)


### Features

* **GlModal:** add modal scrollability guidelines ([6334f20](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/6334f20352ab656e389d690754b7802148cdbe0a))

# [1.27.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.26.0...v1.27.0) (2020-07-15)


### Features

* **broadcast:** Add anchor styling specification ([850e88d](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/850e88dcae280cb907a0517aa0e1bc9af812f83a))

# [1.26.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.25.1...v1.26.0) (2020-07-15)


### Features

* **card:** Add details for when and when not to use cards with examples ([e8d23d4](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/e8d23d4df0bc4e61f0ae09c48402cb7fb07f5332))

## [1.25.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.25.0...v1.25.1) (2020-07-09)


### Bug Fixes

* **font-awesome:** Remove font awesome from repo ([26d2e47](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/26d2e47ad8ccc3e391ca24d100a8bb838dc72ec8))

# [1.25.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.24.0...v1.25.0) (2020-07-08)


### Features

* **interaction:** Adds recommendations on external links ([ebd6221](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/ebd62218e1f173b22a9bc2820aacbe5bffaf364a))

# [1.24.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.23.0...v1.24.0) (2020-07-08)


### Features

* **badge:** Add badge demos ([985181a](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/985181acfcb89ada0093c48e4d8cdcbdbce0d2df))
* **dropdowns:** Add dropdown demos ([e3222cf](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/e3222cfa3fad869b0adea62270e68174b9030314))
* **infinite-scroll:** Mark docs complete ([b11915d](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/b11915d20ed66de845c3e5eb3652961b36c048e6))
* **progess-bar:** Add todo back for demo ([d9d04d2](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/d9d04d2cbb61342a2455004723395cd158b30d71))
* **progress-bar:** Add demo to progress bar ([869674e](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/869674ee209a59705c89ad865c35e3ac649dc43f))
* **table:** Add demo to tables ([4482eda](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/4482edaf5ac0fb713a64be867287d197a440c6c1))
* **toast:** Add demos to toast ([03a16b3](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/03a16b3d375922cfa29eb41aefccd383d91ddde0))
* **toast:** Change docs status to in progress ([b613071](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/b6130715c06b16ec526ae19752756d97c5ede77d))

# [1.23.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.22.0...v1.23.0) (2020-07-07)


### Features

* **file-uploader:** Add design specs ([e431a67](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/e431a67dda22b0bbafc0992a51fea06e4bd6b203))

# [1.22.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.21.4...v1.22.0) (2020-06-29)


### Features

* **Path:** Add Path component documentation ([a5073af](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/a5073af30722d289a9de3f2b6f76dcf51f7d1687))

## [1.21.4](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.21.3...v1.21.4) (2020-06-26)


### Bug Fixes

* **charts:** Moves charts and data viz components to data viz section ([d3b7cc8](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/d3b7cc80bdf2ac11dc9cecf8767582e53adfad6a))

## [1.21.3](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.21.2...v1.21.3) (2020-06-26)


### Bug Fixes

* Added objects to PJs structure ([ae96b88](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/ae96b88e8ff65d8e1b407d33ac8c397dca828392))

## [1.21.2](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.21.1...v1.21.2) (2020-06-25)


### Bug Fixes

* **research:** Correct research links in get started page ([263d29c](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/263d29cecd2a8508f0f6211346c5099bfc591957))

## [1.21.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.21.0...v1.21.1) (2020-06-25)


### Bug Fixes

* **typography:** Remove done todo related to showcasing bold variable ([ca2c3cb](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/ca2c3cb70efb3984cfec4b5c4ead910242ffccfd))

# [1.21.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.20.0...v1.21.0) (2020-06-23)


### Features

* add infinite scroll examples ([abdd198](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/abdd198adec425c6807264c9fc782e9d76737089))

# [1.20.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.19.0...v1.20.0) (2020-06-23)


### Features

* **Color:** Document $purple- color variables ([4599d05](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/4599d05f6714d81627d2095fb6fa298b87626640))

# [1.19.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.18.1...v1.19.0) (2020-06-19)


### Features

* **typography:** Add details about type scales ([ba524c2](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/ba524c2c43c0f44f67638d03644b3ccbe8085ebf))

## [1.18.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.18.0...v1.18.1) (2020-06-18)


### Bug Fixes

* **table:** Update & simplify table documentation ([22f1667](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/22f16674e5e89006e88d4175fb09a9feb2c359eb))

# [1.18.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.17.1...v1.18.0) (2020-06-18)


### Features

* **toggles:** Update usage for left aligned labels ([1720a6b](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/1720a6be8010f2b8c5479efeffb0c849f856a4cf))

## [1.17.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.17.0...v1.17.1) (2020-06-16)


### Bug Fixes

* Resolve status.vue compiler issue ([5bc082a](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/5bc082a611b4c89588d4d4ba857a5b919b059789))

# [1.17.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.16.0...v1.17.0) (2020-06-15)


### Features

* **statustable:** Add gitlab ui link to mark toggles complete ([577ca1f](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/577ca1f90e4a30a83dbee9582ec090232a6e6464))

# [1.16.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.15.1...v1.16.0) (2020-06-15)


### Features

* Add hover state to responsive hamburger menu ([8fe57cb](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/8fe57cb0d1595024e2c38e6c3f16c141118bd229))

## [1.15.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.15.0...v1.15.1) (2020-06-12)


### Bug Fixes

* Add max width to page for better readability ([4ea9588](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/4ea95882c34329da5cdd04f8706ce36e5bdfb90f))

# [1.15.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.14.0...v1.15.0) (2020-06-10)


### Features

* **charts:** Add more options menu documentation ([4561b2a](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/4561b2a596583402a9e20a36fbcbbaac48f69b98))

# [1.14.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.13.0...v1.14.0) (2020-06-09)


### Features

* **statustable:** Use gitlab badges for status table ([a1fa460](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/a1fa4605638f30397a8f353b9764229cc9a6e29d))

# [1.13.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.12.2...v1.13.0) (2020-06-09)


### Features

* **tables:** Update table documentation layout ([1ba10f4](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/1ba10f46c70b0328586b90ab8d5539d4a6b38bd6))

## [1.12.2](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.12.1...v1.12.2) (2020-06-05)


### Bug Fixes

* **Sorting:** Reformat docs only, no content change ([186a610](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/186a6103b8bf4af5a0741cc091ac2e076b554018))

## [1.12.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.12.0...v1.12.1) (2020-05-28)


### Bug Fixes

* Fix query url for vue ([ac11d9f](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/ac11d9fae1671e5cccf930c6239d27c05d0d9840))

# [1.12.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.11.0...v1.12.0) (2020-05-27)


### Features

* Add param for vue component section ([879f0ea](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/879f0ea7a75a456396118030ea2d91e4c8220e70))

# [1.11.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.10.3...v1.11.0) (2020-05-27)


### Features

* **report:** Add report object documentation ([7293914](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/7293914f01ae4db15d67a420d98d61bdab7e4fcb))

## [1.10.3](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.10.2...v1.10.3) (2020-05-27)


### Bug Fixes

* **file-uploader:** Fix casing to ensure Todo banner is correctly parsed ([e9732b3](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/e9732b3386de7caeb8d001157c3031b921b6d7b6))

## [1.10.2](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.10.1...v1.10.2) (2020-05-25)


### Bug Fixes

* **file-uploader:** Remove broken anchors ([c430214](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/c430214435bbe60bab3579258fb3742202a51fbb))

## [1.10.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.10.0...v1.10.1) (2020-05-22)


### Bug Fixes

* **homepage:** Updated link to Figma UI Kit ([9d3eb22](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/9d3eb224b39a9a0b1aa99282c989727509ea6f95))

# [1.10.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.9.0...v1.10.0) (2020-05-21)


### Features

* **tree:** Add design spec link for Tree component ([0e8def7](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/0e8def726a393f86224623c7b645340df7c99edd))

# [1.9.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.8.0...v1.9.0) (2020-05-20)


### Features

* **Sorting:** Refining sorting vs filtering guidelines ([53f1543](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/53f154311cdef36c08723d5eb40ce5a353e1830e))

# [1.8.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.7.0...v1.8.0) (2020-05-20)


### Features

* **progressbar:** Add documentation for progress bar ([cd2739b](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/cd2739b651cffe2ca6e82ff9e7dafcdef21d2674))

# [1.7.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.6.0...v1.7.0) (2020-05-20)


### Features

* **banner:** Add dismissal guidelines ([1dd8338](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/1dd8338148342f41ecd75fcb900f481e3fc373cf))

# [1.6.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.5.1...v1.6.0) (2020-05-20)


### Features

* **token:** Add Pajamas UI Kit link for tokens ([296ffa1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/296ffa175ca182855d08f8648b9f5f9bf4e63265))

## [1.5.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.5.0...v1.5.1) (2020-05-15)


### Bug Fixes

* **Pajamas UI Kit:** Remove beta reference to correct resource anchor ([da74802](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/da74802b144d5e3f97828ce8df5e1ed8001b2b72))

# [1.5.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.4.0...v1.5.0) (2020-05-14)


### Features

* **fileuploader:** Add documentation for file uploader ([9726a98](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/9726a98dd79629b82c97984f686b50cb33e48193))

# [1.4.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.3.0...v1.4.0) (2020-05-14)


### Features

* **filter:** Add vue component link and component example ([450ac8e](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/450ac8e311ed6198abc175f0ffab6c6770ea1697))

# [1.3.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.2.1...v1.3.0) (2020-05-14)


### Features

* **sorting:** Add Pajamas UI Kit link for sorting component ([b94ef04](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/b94ef04af5f4e03ade50463c353d370a8eef6fbc))

## [1.2.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.2.0...v1.2.1) (2020-05-14)


### Bug Fixes

* **css:** Rename colour gray-0 to gray-10 ([f2376c8](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/f2376c8375f6dee1e87e46eb02e1ca4b06cf97f3))

# [1.2.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.1.1...v1.2.0) (2020-05-13)


### Features

* **statustable:** Add new columns to component status table ([63dc07b](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/63dc07b4d61268f1dc59157471a0bfeeff99daea))

## [1.1.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.1.0...v1.1.1) (2020-05-12)


### Bug Fixes

* Update design spec links for several components ([d7755a1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/d7755a1920a65e703027c9236abe64899cbd3c4d))

# [1.1.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.0.0...v1.1.0) (2020-05-08)


### Features

* **buttons:** Add position exception for alerts ([f12bb3d](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/f12bb3d555b65c8fa565ffac109cc436ed032322))

# 1.0.0 (2020-04-29)


### Bug Fixes

* Support links to Markdown pages ([8a8e87b](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/8a8e87bf303b481a2dc4eb25b79a3a258868a25f))
